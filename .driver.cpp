#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>

#include "grid_world.h"

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::map;
using std::vector;

int main() 
{
    map<string, string> sensory_input;
    grid_world::init_sensory_input(sensory_input);

    int ROW = 0;
    int COL = 0;
    int MAX = 0;
    vector<vector<int> > grid;
    grid_world::init_grid(grid, ROW, COL, MAX);

    cout << "\nEnter robot's input: ";
    string input = ""; 
    cin >> input;
    std::transform(input.begin(), input.end(), input.begin(), ::toupper);

    while (sensory_input.count(input) == 0)
    {
        cout << "\nEnter a valid input for the robot: ";
        cin >> input;
        std::transform(input.begin(), input.end(), input.begin(), ::toupper);
    }

    vector<vector<double> > T(MAX, vector<double>(MAX, 0));
    vector<string> direction_possibilities(MAX, "");    
    grid_world::calculate_T(grid, direction_possibilities, T, ROW, COL); 
    
    cout << "\nDirection possibilities: " << endl;
    grid_world::print_1d_vector(direction_possibilities);

    // Compare each direction possibility to robot input; calculating d
    vector<int> d(MAX, 0);
    grid_world::calculate_d(direction_possibilities, sensory_input, MAX, input, d);

    cout << "\nd: " << endl;
    grid_world::print_1d_vector(d);

    cout << "\nT:" << endl;
    grid_world::print_2d_vector(T);

    vector<double> P_e_X(MAX, 0);
    float e = 0.1;
    grid_world::calculate_P_e_X(P_e_X, e, d, MAX);

    cout << "\nP(e|X):" << endl;
    grid_world::print_1d_vector(P_e_X);

    vector<double> F(MAX, 0);
    grid_world::calculate_F(F, MAX);

    cout << "\nF:" << endl;
    grid_world::print_1d_vector(F);

    // Calculate R = T^T (transpose of T)
    vector<vector<double> > R(MAX, vector<double>(MAX, 0));    
    grid_world::calculate_R(R, T, MAX);

    cout << "\nR:" << endl;
    grid_world::print_2d_vector(R);

    // Calculate Y = R x F
    vector<double> Y(MAX, 0);
    grid_world::calculate_Y(Y, R, F, MAX);

    cout << "\nY:" << endl;
    grid_world::print_1d_vector(Y);

    vector<vector<double> > O(MAX, vector<double>(MAX, 0));
    grid_world::calculate_O(O, P_e_X, MAX);

    cout << "\nO:" << endl;
    grid_world::print_2d_vector(O);

    // Calculate Z = O x Y
    vector<double> Z(MAX, 0);
    grid_world::calculate_Z(Z, O, Y, MAX);

    double Z_sum = 0;
    grid_world::sum_Z(Z, Z_sum, MAX);
    
    cout << "\nZ's sum: " << endl;
    cout << Z_sum << endl;
    
    cout << "\nZ:" << endl;
    grid_world::print_1d_vector(Z);

    // Update F w/ probabilities
    // F[i][1] = Z[i][1]/sum
    grid_world::update_F(F, Z, Z_sum, MAX);

    cout << "\nUpdated F: " << endl;
    grid_world::print_updated_F(F, MAX);

    double prob_max = 0.0;
    vector<int> probable_cells;
    grid_world::find_all_cells_with_max_probability(F, probable_cells, prob_max, MAX);

    grid_world::print_determined_probabilities(probable_cells, sensory_input);

    return 0;
}
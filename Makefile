gl: grid_world.o driver.o
	g++ -Wall -pedantic -g -o gl grid_world.o driver.o

grid_world.o: grid_world.h grid_world.cpp
	g++ -Wall -pedantic -g -c grid_world.cpp

driver.o: grid_world.o
	g++ -Wall -pedantic -g -c driver.cpp

clean:
	rm -f gl gl.exe *.o
Mobile Robot Global Localization
================================

Global localization is the problem of determining the position of a robot under global uncertainty. This program solves this by:

* taking a sequence of sensory inputs of a mobile robot living in a grid world (2D vector)
    * Sensory input follows any ordered combination of NSWE separated by a whitespace. The N, S, W, and E direction specified by the input indicates a roadblock in said direction. So if the input were simply, "NSW", we know that the mobile robot can only move East. If "S", the mobile robot can only North, West, and East.
* calculating the probabilities of the mobile robot residing in each cell of the grid world 
* and outputting the largest probabilities (with corresponding locations). 

By solving this problem, we can figure out the most likely cell(s) the mobile robot is currently residing in given its sensory input.

---

_Example grid world:_ (cells with a roadblock contain 0)
    
    1  2  3  4  5
    6  0  7  0  8
    9 10 11 12 13

_Example input:_ "**NS N**". Based on the first sensory input, "NS", we look for the cells in the grid world in which the robot can _only_ move West and East. We find these cells to be 2, 4, 10, and 12. Based on the second sensory input, "N", we now look for the cells in which the robot can only move South, West, or East. We find the most likely cell is 3. Given the sequence, 3 also turns out the be the most likely cell the mobile robot is in. 

For each iteration of calculating the probabilities for each cell (stored in the 1D vector, "F"), we recursively use the previous values of F for the current calculations. Each sensory input affects our determining the next.

Notes
-----

* Presently, there are two grid world input files. The first two number represent the height and width of the 2D vector. The following lines make up each row of the actual vector. The input file is hardcoded in grid_world::init_grid(). You can write your own or use the examples. 
* .driver.cpp is a one-iteration test program printing out each step of the calculations. Regarding driver.cpp you can use grid_world::print_1d_vector(<1d vector>) or grid_world::print_2d_vector(<2d vector>) to display the calculations' progress. Currently, the final probability table, F, is only outputted.

Build & Run
-----------

1. Compile: $ `make`
2. Run: $ `./gl`
3. `Ctrl+D` to end input

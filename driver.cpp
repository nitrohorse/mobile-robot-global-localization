#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>

#include "grid_world.h"

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::map;
using std::vector;

int main()
{
    map<string, string> sensory_input;
    grid_world::init_sensory_input(sensory_input);

    int ROW = 0;
    int COL = 0;
    int MAX = 0;
    vector<vector<int> > grid;
    grid_world::init_grid(grid, ROW, COL, MAX);
    vector<vector<double> > T(MAX, vector<double>(MAX, 0));
    vector<string> direction_possibilities(MAX, "");
    vector<int> d(MAX, 0);
    vector<double> P_e_X(MAX, 0);
    vector<double> F(MAX, 0);
    vector<vector<double> > R(MAX, vector<double>(MAX, 0));
    vector<double> Y(MAX, 0);
    vector<vector<double> > O(MAX, vector<double>(MAX, 0));
    vector<double> Z(MAX, 0);
    double Z_sum = 0;
    float e = 0.1;
    vector<string> input_sequence;

    cout << "\nEnter robot's input: ";
    string input = "";
    while (cin >> input) // Ctrl-D to end input
    {
        std::transform(input.begin(), input.end(), input.begin(), ::toupper);

        if (sensory_input.count(input) == 0)
        {
            cout << "\nEnter a valid input: ";
        }
        else
        {
            input_sequence.push_back(input);
        }
    }

    grid_world::calculate_F(F, MAX);
    grid_world::calculate_T(grid, direction_possibilities, T, ROW, COL);
    grid_world::calculate_R(R, T, MAX);

    for (size_t i = 0; i < input_sequence.size(); ++i)
    {
        grid_world::calculate_d(direction_possibilities, sensory_input, MAX, input_sequence[i], d);
        grid_world::calculate_P_e_X(P_e_X, e, d, MAX);
        grid_world::calculate_Y(Y, R, F, MAX);
        grid_world::calculate_O(O, P_e_X, MAX);
        grid_world::calculate_Z(Z, O, Y, MAX);
        grid_world::sum_Z(Z, Z_sum, MAX);
        grid_world::update_F(F, Z, Z_sum, MAX);

        // re-init vectors
        T.resize(MAX, vector<double>(MAX, 0));
        direction_possibilities.resize(MAX, "");
        d.resize(MAX, 0);
        P_e_X.resize(MAX, 0);
        R.resize(MAX, vector<double>(MAX, 0));
        Y.resize(MAX, 0);
        O.resize(MAX, vector<double>(MAX, 0));
        Z.resize(MAX, 0);
    }
    cout << "\nUpdated F: " << endl;
    grid_world::print_updated_F(F, MAX);

    double prob_max = 0.0;
    vector<int> probable_cells;
    grid_world::find_all_cells_with_max_probability(F, probable_cells, prob_max, MAX);

    cout << "\nGiven ";
    for (size_t i = 0; i < input_sequence.size(); ++i)
    {
        if (i != 0)
        {
            cout << ", ";
        }
        cout << input_sequence[i] << " (" << sensory_input[input_sequence[i]] << ")";
    }
    cout << "...";
    grid_world::print_determined_probabilities(probable_cells, sensory_input);
    return 0;
}
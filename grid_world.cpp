#include "grid_world.h"

namespace grid_world
{
    void init_sensory_input(std::map<std::string, std::string>& sensory_input)
    {
        sensory_input["E"]    = "0001";
        sensory_input["W"]    = "0010";
        sensory_input["WE"]   = "0011";
        sensory_input["S"]    = "0100";
        sensory_input["SE"]   = "0101";
        sensory_input["SW"]   = "0110";
        sensory_input["SWE"]  = "0111";
        sensory_input["N"]    = "1000";
        sensory_input["NE"]   = "1001";
        sensory_input["NW"]   = "1010";
        sensory_input["NWE"]  = "1011";
        sensory_input["NS"]   = "1100";
        sensory_input["NSE"]  = "1101";
        sensory_input["NSW"]  = "1110";
        sensory_input["NSWE"] = "1111";
    }

    void init_grid(std::vector<std::vector<int> >& grid, int& ROW, int& COL, int& MAX)
    {
        std::ifstream inf("in2.txt");
        inf >> ROW >> COL;
        grid.resize(ROW, std::vector<int>(COL, 0));
        int cell = 0;
        std::cout << "\nThe grid world:" << std::endl;
        for (int i = 0; i < ROW; ++i)
        {
            for (int j = 0; j < COL; ++j)
            {
                inf >> cell;
                grid[i][j] = cell;
                if (cell > MAX)
                {
                    MAX = cell;
                }
                std::cout << grid[i][j] << ' ';
            }
            std::cout << std::endl;
        }
        inf.close();
    }

    void calculate_T(const std::vector<std::vector<int> >& grid, std::vector<std::string>& direction_possibilities, std::vector<std::vector<double> >& T, const int& ROW, const int& COL)
    {
        std::string direction_builder = "";
        int count_of_neighbors = 0;
        // std::cout << "\nNeighbors of each cell:" << std::endl;
        for (int i = 0; i < ROW; ++i)
        {
            for (int j = 0; j < COL; ++j)
            {
                if (grid[i][j] != 0)
                {
                    // std::cout << "Current cell: " << grid[i][j] << std::endl;
                    // if in bounds and neighbor is NOT a road block
                    if (i-1 >= 0 && grid[i-1][j] != 0) /* top */
                    {
                        // std::cout << "Top: " << grid[i-1][j] << std::endl;
                        count_of_neighbors++;
                        direction_builder += "0";
                    }
                    else
                    {
                        direction_builder += "1";
                    }

                    if (i+1 < ROW && grid[i+1][j] != 0) /* bottom */
                    {
                        // std::cout << "Bottom: " << grid[i+1][j] << std::endl;
                        count_of_neighbors++;
                        direction_builder += "0";
                    }
                    else
                    {
                        direction_builder += "1";
                    }

                    if (j-1 >= 0 && grid[i][j-1] != 0) /* left */
                    {
                        // std::cout << "Left: " << grid[i][j-1] << std::endl;
                        count_of_neighbors++;
                        direction_builder += "0";
                    }
                    else
                    {
                        direction_builder += "1";
                    }

                    if (j+1 < COL && grid[i][j+1] != 0) /* right */
                    {
                        // std::cout << "Right: " << grid[i][j+1] << std::endl;
                        count_of_neighbors++;
                        direction_builder += "0";
                    }
                    else
                    {
                        direction_builder += "1";
                    }

                    // std::cout << "Sensory input: " << direction_builder << std::endl;
                    direction_possibilities[grid[i][j]-1] = direction_builder;
                    direction_builder = "";
                }
                // std::cout << "Count of neighbors: " << count_of_neighbors << std::endl;

                if (count_of_neighbors != 0)
                {
                    double trans_prob = (double)1/count_of_neighbors;

                    if (i-1 >= 0 && grid[i-1][j] != 0)
                    {
                        T[grid[i][j]-1][grid[i-1][j]-1] = trans_prob;
                    }

                    if (i+1 < ROW && grid[i+1][j] != 0)
                    {
                        T[grid[i][j]-1][grid[i+1][j]-1] = trans_prob;
                    }

                    if (j-1 >= 0 && grid[i][j-1] != 0)
                    {
                        T[grid[i][j]-1][grid[i][j-1]-1] = trans_prob;
                    }

                    if (j+1 < COL && grid[i][j+1] != 0)
                    {
                        T[grid[i][j]-1][grid[i][j+1]-1] = trans_prob;
                    }
                    count_of_neighbors = 0;
                }
            }
        }
    }

    void calculate_d(const std::vector<std::string>& direction_possibilities, std::map<std::string, std::string>& sensory_input, const int& MAX, const std::string& input, std::vector<int>& d)
    {
        int count = 0;
        for (int i = 0; i < MAX; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                if (direction_possibilities[i][j] != sensory_input[input][j])
                {
                    count++;
                }
            }
            d[i] = count;
            count = 0;
        }
    }

    void calculate_P_e_X(std::vector<double>& P_e_X, const float& e, std::vector<int>& d, const int& MAX)
    {
        for (int i = 0; i < MAX; ++i)
        {
            // P(e|X) = e^d * (1-e)^(4-d)
            P_e_X[i] = pow(e,d[i]) * pow((1-e), (4-d[i]));
        }
    }

    void calculate_F(std::vector<double>& F, const int& MAX)
    {
        for (int i = 0; i < MAX; ++i)
        {
            F[i] = (double)1/MAX;
        }
    }

    void calculate_R(std::vector<std::vector<double> >& R, const std::vector<std::vector<double> >& T, const int& MAX)
    {
        for (int i = 0; i < MAX; ++i)
        {
            for (int j = 0; j < MAX; ++j)
            {
                R[i][j] = T[j][i];
            }
        }
    }

    void calculate_Y(std::vector<double>& Y, const std::vector<std::vector<double> >& R, const std::vector<double>& F, const int& MAX)
    {
        for (int i = 0; i < MAX; ++i)
        {
            for (int j = 0; j < MAX; ++j)
            {
                if (R[i][j] != 0)
                {
                    Y[j] += R[i][j] * F[j];
                }
            }
        }
    }

    void calculate_O(std::vector<std::vector<double> >& O, const std::vector<double>& P_e_X, const int& MAX)
    {
        for (int i = 0; i < MAX; ++i)
        {
            for (int j = 0; j < MAX; ++j)
            {
                if (i == j)
                {
                    O[i][j] = P_e_X[i];
                }
            }
        }
    }

    void calculate_Z(std::vector<double>& Z, const std::vector<std::vector<double> >& O, const std::vector<double>& Y, const int& MAX)
    {
        for (int i = 0; i < MAX; ++i)
        {
            Z[i] = O[i][i] * Y[i];
        }
    }

    void sum_Z(const std::vector<double>& Z, double& Z_sum, const int& MAX)
    {
        for (int i = 0; i < MAX; ++i)
        {
            Z_sum += Z[i];
        }
    }

    void update_F(std::vector<double>& F, const std::vector<double>& Z, const double& Z_sum, const int& MAX)
    {
        for (int i = 0; i < MAX; ++i)
        {
            F[i] = Z[i]/Z_sum;
        }
    }

    void print_updated_F(const std::vector<double>& F, const int& MAX)
    {
        for (int i = 0; i < MAX; ++i)
        {
            if (i+1 >= 10)
            {
                std::cout << i+1 << " | " << F[i] << std::endl;
            }
            else
            {
                std::cout << i+1 << "  | " << F[i] << std::endl;
            }
        }
    }

    void find_all_cells_with_max_probability(const std::vector<double>& F, std::vector<int>& probable_cells, double& prob_max, const int& MAX)
    {
        for (int i = 0; i < MAX; ++i)
        {
            if (F[i] > prob_max)
            {
                prob_max = F[i];
            }
        }
        for (int i = 0; i < MAX; ++i)
        {
            if (F[i] == prob_max)
            {
                probable_cells.push_back(i+1);
            }
        }
    }

    void print_determined_probabilities(const std::vector<int>& probable_cells, std::map<std::string, std::string>& sensory_input)
    {
        std::cout << "\nThe robot is most likely in";

        if (probable_cells.size() > 1)
        {
            std::cout << " cells ";
        }
        else
        {
            std::cout << " cell ";
        }
        for (size_t i = 0; i < probable_cells.size(); ++i)
        {
            if (i != 0)
            {
                std::cout << ", ";
            }
            std::cout << probable_cells[i];
        }
        std::cout << "." << std::endl << std::endl;
    }
}
#include <iostream>
#include <cmath>
#include <string>
#include <map>
#include <vector>
#include <fstream>

namespace grid_world 
{
    void init_sensory_input(std::map<std::string, std::string>& sensory_input);
    void init_grid(std::vector<std::vector<int> >& grid, int& ROW, int& COL, int& MAX);
    void calculate_T(const std::vector<std::vector<int> >& grid, std::vector<std::string>& direction_possibilities, std::vector<std::vector<double> >& T, const int& ROW, const int& COL); 
    void calculate_d(const std::vector<std::string>& direction_possibilities, std::map<std::string, std::string>& sensory_input, const int& MAX, const std::string& input, std::vector<int>& d);
    void calculate_P_e_X(std::vector<double>& P_e_X, const float& e, std::vector<int>& d, const int& MAX);
    void calculate_F(std::vector<double>& F, const int& MAX);
    void calculate_R(std::vector<std::vector<double> >& R, const std::vector<std::vector<double> >& T, const int& MAX);
    void calculate_Y(std::vector<double>& Y, const std::vector<std::vector<double> >& R, const std::vector<double>& F, const int& MAX);
    void calculate_O(std::vector<std::vector<double> >& O, const std::vector<double>& P_e_X, const int& MAX);
    void calculate_Z(std::vector<double>& Z, const std::vector<std::vector<double> >& O, const std::vector<double>& Y, const int& MAX);
    void sum_Z(const std::vector<double>& Z, double& Z_sum, const int& MAX);    
    void update_F(std::vector<double>& F, const std::vector<double>& Z, const double& Z_sum, const int& MAX);
    void print_updated_F(const std::vector<double>& F, const int& MAX);
    void find_all_cells_with_max_probability(const std::vector<double>& F, std::vector<int>& probable_cells, double& prob_max, const int& MAX);
    void print_determined_probabilities(const std::vector<int>& probable_cells, std::map<std::string, std::string>& sensory_input);
    
    template <typename T> 
    void print_2d_vector(const std::vector<std::vector<T> >& vec)
    {
        for (size_t i = 0; i < vec.size(); ++i)
        {
            for (size_t j = 0; j < vec[0].size(); ++j)
            {
                std::cout << vec[i][j] << ' ';
            }
            std::cout << std::endl;
        }          
    }
    
    template <typename T> 
    void print_1d_vector(const std::vector<T>& vec)
    {
        for (size_t i = 0; i < vec.size(); ++i)
        {
            if (i+1 >= 10)
            {
                std::cout << i+1 << " | " << vec[i] << std::endl;
            }
            else 
            {
                std::cout << i+1 << "  | " << vec[i] << std::endl;
            }
        }        
    }
}